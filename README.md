## Thunderbolt model rocket

Thunderbolt is a model rocket built in [Hackerspace.gr](https://www.hackerspace.gr/#). It's mission is to collect flight data to a custom designed data-logger in KiCAD. Firstly, the avionics system will run on a Nucleo board. Additionally, an IMU and a barometric altimeter will be used. It is well worth mentioning that, the drivers for the sensors will be written from scratch and will be included in this repository. Lastly, the diameter of the rocket is small, thus a design of a fairing is needed to fit the avionics bay. The fairing will be 3D printed.
